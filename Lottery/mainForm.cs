﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using LotteryLib;

namespace Lottery {
    public partial class mainForm : Form {
        public mainForm() {
            InitializeComponent();
        }

        private void mainForm_Load(object sender, EventArgs e) {
            if(!File.Exists("./data.db")){
                //MessageBox.Show("无数据库文件");
                try {
                    this.CreateDB();
                }
                catch (Exception ex) {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
        /// <summary>
        /// 创建历史数据文件
        /// </summary>
        /// <returns>是否成功</returns>
        private bool CreateDB() {
            DoubleLottery DL = new DoubleLottery();
            DL.GetDB();
            return true;
        }
    }
}
