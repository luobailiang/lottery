﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LotteryLib {
    class FetchFactory {
        private static LotterySource LS = null;

        public static LotterySource Init(string FetchType) {
            switch (FetchType) {
                case "zhcw":
                    if (LS == null) {
                        try {
                            LS = new zhcwSource();
                        }
                        catch {
                            LS = null;
                        }
                    }
                    else {
                        throw new Exception();
                    }
                    break;
                case "sina":
                    if (LS == null) {
                        try {
                            LS = new SinaSource(); 
                        }
                        catch {
                            LS = null;
                        }
                    }
                    else {
                        throw new Exception();
                    }
                    break;
            }
            return LS;
        }
    }
}
